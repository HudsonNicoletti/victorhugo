<?php

namespace Website\Models;

class Portfolio extends \Phalcon\Mvc\Model
{
  public function initialize()
  {
      $this->setSource("portfolio");
  }
}

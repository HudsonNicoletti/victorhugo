<?php
/**
 * @author Hudson Nicoletti <hudsonnicoletti.com>
 * @copyright Copyright (c) 2021, Hudson Nicoletti
 * @version 3.0
**/
namespace Website\Controllers;

use Website\Models\Portfolio    as Portfolio;

class IndexController extends ControllerBase
{
  public function IndexAction()
  {
    $this->view->items = Portfolio::find();
    $this->view->pick("{$this->website_lang}/index/index");
  }

  public function PortfolioItemAction()
  {
    $slug = $this->dispatcher->getParam('slug');
    $query   = Portfolio::findFirstBySlug($slug);
    $related = Portfolio::find([
      "limit" => 3
    ]);

    if (!$query) {
      $this->response->redirect("/404");
    }

    $this->view->item  = $query;
    $this->view->related  = $related;
    $this->view->pick("{$this->website_lang}/index/portfolioItem");
  }

  public function AboutAction()
  {
    $this->view->pick("{$this->website_lang}/about/index");
  }

  public function ContactAction()
  {
    $this->view->pick("{$this->website_lang}/contact/index");
  }


  public function SendAction()
  {
    $this->response->setContentType("application/json");
    $response = "";

      # send email
      $this->mail->FromName   = "hudson19962009@gmail.com";

      $this->mail->addAddress("hudson19962009@gmail.com",  "hudson");
      $this->mail->Subject = " Entrou em contato pelo site!";

      $this->mail->Body = $this->request->getPost("message","string");

      if ($this->mail->smtpConnect())
      {

        if(!$this->mail->send())
        {
          $response = "Não foi possível enviar, tente novamente";
        }
        else{
          $response = "Enviado com Sucesso!";
        }

      $this->mail->ClearAddresses();
      $this->mail->smtpClose();

      }

      return $this->response->setJsonContent([
        "response" =>  $response
      ]);

      $this->response->send();
      $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}

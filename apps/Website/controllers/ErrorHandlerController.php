<?php
/**
 * @author Hudson Nicoletti <hudsonnicoletti.com>
 * @copyright Copyright (c) 2021, Hudson Nicoletti
 * @version 3.0
**/
namespace Website\Controllers;

class ErrorHandlerController extends ControllerBase
{
  public function ServerErrorAction()
  {
    $this->assets->addCss('/assets/css/error.css');
    $this->view->pick("error/500");
  }

  public function NotFoundAction()
  {
    $this->assets->addCss('/assets/css/error.css');
    $this->view->pick("error/404");
  }
}

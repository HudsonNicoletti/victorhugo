<?php
/**
 * @author Hudson Nicoletti <hudsonnicoletti.com>
 * @copyright Copyright (c) 2021, Hudson Nicoletti
 * @version 3.0
**/
namespace Website\Controllers;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
  protected $website_lang;

  public function onConstruct()
  {
      if($this->cookies->has("website_lang"))
      {
        $this->website_lang = $this->cookies->get("website_lang")->getValue();
      }
      else {
        $this->setBestLangauge();
      }
  }

  public function initialize()
  {

    $this->assets
    ->addCss('https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css',false)
    ->addCss('https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css')
    ->addCss('https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css')
    ->addCss('/assets/css/main.css')
    ->addJs('https://code.jquery.com/jquery-3.6.0.min.js',false)
    ->addJs('https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js')
    ->addJs('https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js')
    ->addJs('/assets/js/filtr.js')
    ->addJs('/assets/js/app.js');

  }

  public function setBestLangauge()
  {
    $lang = substr($this->request->getBestLanguage(),0,2);

    $new_lang = $this->changeLanguage($lang);

    return $new_lang;
  }

  public function changeLanguage($language)
  {
    switch($language)
    {
      case 'pt': $this->cookies->set("website_lang","pt"); break;
      case 'en': $this->cookies->set("website_lang","en"); break;
      default:   $this->cookies->set("website_lang","en"); break;
    }

    return $this->response->redirect();

  }

  protected function isEmail($e)
  {
    return preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $e );
  }

  protected function uniqueCode($prefix = null, $limit = 10)
  {
    $limit = ($prefix ? $limit - strlen($prefix) : $limit);

    return $prefix.str_shuffle(substr(md5(round(time().uniqid())), 0, $limit));
  }

  protected function makeSlug($string){
    return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
  }

}

<?php
/**
 * @author Hudson Nicoletti <hudsonnicoletti.com>
 * @copyright Copyright (c) 2021, Hudson Nicoletti
 * @version 3.0
**/
namespace Website\Controllers;

use Phalcon\Mvc\Controller;

class LanguageController extends ControllerBase
{
  public function ChangeAction()
  {
    $language_set = $this->changeLanguage($this->dispatcher->getParam("language"));

    $this->response->redirect("/");
  }
}

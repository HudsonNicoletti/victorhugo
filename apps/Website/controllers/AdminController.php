<?php
/**
 * @author Hudson Nicoletti <hudsonnicoletti.com>
 * @copyright Copyright (c) 2021, Hudson Nicoletti
 * @version 3.0
**/
namespace Website\Controllers;

use Website\Models\Users    as Users,
    Website\Models\Portfolio    as Portfolio;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Hidden;

class AdminController extends ControllerBase
{

  public function indexAction()
  {
    $this->view->pick("admin/index");

    $this->view->items = Portfolio::find();
  }

  public function LoginAction()
  {
    $this->view->pick("admin/login");
    $form = new Form();

    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken()
    ]);

    foreach ($element as $e)
    {
      $form->add($e);
    }

    $this->view->form = $form;
  }

  public function AuthAction()
  {
    $username   = preg_replace('/\s+/', '', $this->request->getPost("username","string"));  # Get username input & filter as a string
    $password   = preg_replace('/\s+/', '', $this->request->getPost("password","string"));  # Get password input & filter as a string
    $user       = ( !$this->isEmail( $username ) ) ? Users::findFirstByUsername($username) : Users::findFirstByEmail($username); # check if username input is a valid email or regular username

    try
    {
      if(!$this->request->isPost()):                            # Accept only POST request
        throw new \Exception("Invalid Request") ;                # throws exception if not POST request

      elseif(!$user):                                        # checks if user does not exist
        throw new \Exception("Usuário Inválido") ;                # throws exception if user does not exist

      elseif(!password_verify( $password , $user->password )):  # checks if password input is invalid
        throw new \Exception("Senha Inválida") ;                  # throws exception if password is invalid

      elseif(!$this->security->checkToken()):                   # checks if CSRF token is invalid
        throw new \Exception("Invalid Token") ;                  # throws exception if CSRF token is invalid

      else:

        $this->session->set("vhf_session", $user->_);   # start a session with user id
        return $this->response->redirect('/admin',true);

      endif;

    }
    catch(\Exception $e)
    {
      return $this->response->redirect('/admin?msg='. urlencode($e->getMessage()),true);
    }


    $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);   # set view with no templating , only plain action view for JSON
  }

  /*
  * Handles logout request, terminates current session and redirects to home page
  *
  * @return Phalcon\Http\Response
  */
  public function LogoutAction()
  {
    $this->session->destroy();                      # terminate session
    return $this->response->redirect('/admin',true);     # redirect to index
  }

  public function NewAction()
  {
    $this->assets->addJs('/assets/js/admin.js');

          $form = new Form();

          $element['security'] = new Hidden( "security" ,[
            'name'  => $this->security->getTokenKey(),
            'value' => $this->security->getToken()
          ]);

          $element['title'] = new Text( "title" ,[
            'required' => true,
            'name' => 'title',
            'placeholder' => 'English Title'
          ]);

          $element['file'] = new File( "file" ,[
            'required' => true,
            'name' => 'file',
            'placeholder' => 'Article Image'
          ]);

          $element['text'] = new Textarea( "text" ,[
            'required' => true,
            'name' => 'text',
            'data-editor' => 'editor'
          ]);

          $element['title_pt'] = new Text( "title_pt" ,[
            'required' => true,
            'name' => 'title_pt',
            'placeholder' => 'Portuguese Title'
          ]);

          $element['text_pt'] = new Textarea( "text_pt" ,[
            'required' => true,
            'name' => 'text_pt',
            'data-editor' => 'editor'
          ]);

          $element['filter'] = new Select( "filter" , [
              "LOGO" => "LOGO",
              "IDENTITIES" => "IDENTITIES",
              "WEB" => "WEB",
              "GRAPHIC" => "GRAPHIC",
              "MEDIA" => "MEDIA"
            ],
            [
            'required' => true,
            'name' => 'filter'
          ]);

          foreach ($element as $e)
          {
            $form->add($e);
          }

          $this->view->form = $form;
          $this->view->pick("admin/new");
  }

  public function createAction()
  {
    $this->response->setContentType("application/json");  # set response type of json for ajax                  # use custom functions

    $title   = $this->request->getPost("title","string");
    $text    = $this->request->getPost("text");
    $title_pt   = $this->request->getPost("title_pt","string");
    $text_pt    = $this->request->getPost("text_pt");
    $filter    = $this->request->getPost("filter");

    $response = null;

    try
    {
      if(!$this->request->isPost()):                            # Accept only POST request
        throw new \Exception("Invalid Request") ;                # throws exception if not POST request


      elseif(!$this->request->hasFiles()):
        throw new \Exception("Selecione uma Imagem") ;                # throws exception if user does not exist

      elseif(!$this->security->checkToken()):                   # checks if CSRF token is invalid
        throw new \Exception("Invalid Token") ;                  # throws exception if CSRF token is invalid

      else:

        foreach($this->request->getUploadedFiles() as $file):
          $filename = substr(sha1(uniqid()), 0, 12).'.'.$file->getExtension();
          $file->moveTo("assets/images/{$filename}");
        endforeach;

        $p = new Portfolio;
          $p->title_en    = $title;
          $p->title_pt    = $title_pt;
          $p->slug        = $this->makeSlug($title);
          $p->text_en     = $text;
          $p->text_pt     = $text_pt;
          $p->image       = $filename;
          $p->filter      = $filter;
        if(!$p->save())
        {
          throw new \Exception("Falha ao cadastrar. Tente Novamente.") ;
        }

        $response = [                                # returns flags for ajax
          'response' =>  "Públicado com sucesso!" ,
          'redirect' => "/admin"        # set wellcome message to user
        ];
      endif;

    }
    catch(\Exception $e)
    {
      $response = [            # returns flags for ajax
        'response' => $e->getMessage(),
        'redirect' => "/admin/portfolio/new"
      ];
    }

    return $this->response->setJsonContent($response);   # set json content to flags

    $this->response->send();                                # send content to view
    $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);   # set view with no templating , only plain action view for JSON
  }

  public function editAction()
  {
    $this->assets->addJs('/assets/js/admin.js');

    $slug = $this->dispatcher->getParam('slug');
    $query   = Portfolio::findFirstBySlug($slug);
    if (!$query) {
      $this->response->redirect("/404");
    }

      $form = new Form();

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken()
      ]);

      $element['slug'] = new Hidden( "slug" ,[
        'value' => $slug
      ]);

      $element['title'] = new Text( "title" ,[
        'required' => true,
        'name' => 'title',
        'placeholder' => 'English Title',
        "value" => $query->title_en
      ]);

      $element['file'] = new File( "file" ,[
        'name' => 'file',
        'placeholder' => 'Article Image'
      ]);

      $element['text'] = new Textarea( "text" ,[
        'required' => true,
        'name' => 'text',
        'data-editor' => 'editor',
        "value" => $query->text_en
      ]);

      $element['title_pt'] = new Text( "title_pt" ,[
        'required' => true,
        'name' => 'title_pt',
        'placeholder' => 'Portuguese Title',
        "value" => $query->title_pt
      ]);

      $element['text_pt'] = new Textarea( "text_pt" ,[
        'required' => true,
        'name' => 'text_pt',
        'data-editor' => 'editor',
        "value" => $query->text_pt
      ]);

      $element['filter'] = new Select( "filter" , [
          "LOGO" => "LOGO",
          "IDENTITIES" => "IDENTITIES",
          "WEB" => "WEB",
          "GRAPHIC" => "GRAPHIC",
          "MEDIA" => "MEDIA"
        ],
        [
        'required' => true,
        'name' => 'filter',
        "value" => $query->filter
      ]);

      foreach ($element as $e)
      {
        $form->add($e);
      }

      $this->view->form = $form;
      $this->view->pick("admin/edit");
  }

  public function updateAction()
  {
    $this->response->setContentType("application/json");  # set response type of json for ajax                  # use custom functions

    $slug   = $this->request->getPost("slug");
    $title   = $this->request->getPost("title","string");
    $text    = $this->request->getPost("text");
    $title_pt   = $this->request->getPost("title_pt","string");
    $text_pt    = $this->request->getPost("text_pt");
    $filter    = $this->request->getPost("filter");

    $response = null;

    $p = Portfolio::findFirstBySlug($slug);

    try
    {
      if(!$this->request->isPost()):                            # Accept only POST request
        throw new \Exception("Invalid Request") ;                # throws exception if not POST request

      elseif(!$this->security->checkToken()):                   # checks if CSRF token is invalid
        throw new \Exception("Invalid Token") ;                  # throws exception if CSRF token is invalid

      else:

        if ($this->request->hasFiles()) {
          foreach($this->request->getUploadedFiles() as $file):
            $filename = substr(sha1(uniqid()), 0, 12).'.'.$file->getExtension();
            $file->moveTo("assets/images/{$filename}");
          endforeach;

          @unlink("assets/images/{$p->image}");
        }
        else {
          $filename = $p->image;
        }

          $p->title_en    = $title;
          $p->title_pt    = $title_pt;
          $p->text_en     = $text;
          $p->text_pt     = $text_pt;
          $p->image       = $filename;
          $p->filter      = $filter;
        if(!$p->save())
        {
          throw new \Exception("Falha ao cadastrar. Tente Novamente.") ;
        }

        $response = [                                # returns flags for ajax
          'response' =>  "Alterado com sucesso!" ,
          'redirect' => "/admin"        # set wellcome message to user
        ];
      endif;

    }
    catch(\Exception $e)
    {
      $response = [            # returns flags for ajax
        'response' => $e->getMessage(),
        'redirect' => "/admin/portfolio/edit/{$slug}"
      ];
    }

    return $this->response->setJsonContent($response);   # set json content to flags

    $this->response->send();                                # send content to view
    $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);   # set view with no templating , only plain action view for JSON
  }

  public function removeAction()
  {
    $slug = $this->dispatcher->getParam('slug');
    $query   = Portfolio::findFirstBySlug($slug);
    if (!$query) {
      $this->response->redirect("/404");
    }

    if($query->delete())
    {
      @unlink("assets/images/{$query->image}");
      $this->response->redirect("/admin");
    }
  }
}

<?php
/**
 * @author Hudson Nicoletti <hudsonnicoletti.com>
 * @copyright Copyright (c) 2021, Hudson Nicoletti
 * @version 3.0
**/

namespace Website;

use Phalcon\Loader,
    Phalcon\Mvc\View,
    Phalcon\Mvc\ModuleDefinitionInterface;

class Module
{
  public function registerAutoloaders()
  {
    $loader = new Loader;

    $loader->registerNamespaces([
      'Website\Controllers' => __DIR__ . '/controllers/',
      'Website\Models' => __DIR__ . '/models/'
    ]);

    $loader->register();
  }

  public function registerServices($di)
  {

    $di['view'] = function() {
      $view = new View;
      $view->setViewsDir(__DIR__ . '/views/');

      return $view;
    };

  }
}


$("[data-filter]").on('click', function(){
    $("[data-filter]").removeClass("active");
    $(this).addClass("active");

    var filter = $(this).attr("data-filter");

    if (filter != "") {

        $(`.item [data-id]`).parent().removeClass("animate__animated, animate__fadeIn").addClass("animate__animated animate__fadeOut");
        $(`.item [data-id='${filter}']`).parent().removeClass("animate__animated, animate__fadeOut").addClass("animate__animated animate__fadeIn");

    }
    else
    {
      $(`.item [data-id]`).parent().removeClass("animate__animated, animate__fadeOut").addClass("animate__animated animate__fadeIn");
    }

});

const urlParams = new URLSearchParams(window.location.search);
const myParam = urlParams.get('msg');

if (myParam) {
  $("#response").text(myParam)
}

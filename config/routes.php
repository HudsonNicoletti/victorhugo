<?php

/**
 * @author Hudson Nicoletti <hudsonnicoletti.com>
 * @copyright Copyright (c) 2021, Hudson Nicoletti
 * @version 3.0
**/

/**
  * this file is calling for all routes in the routes folder for better seperation.
  *
**/
$files = glob( __DIR__ . '/routes/*.php');

foreach ($files as $file) {
    require($file);
}

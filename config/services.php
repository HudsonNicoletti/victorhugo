<?php

/**
 * @author Hudson Nicoletti <hudsonnicoletti.com>
 * @copyright Copyright (c) 2021, Hudson Nicoletti
 * @version 3.0
**/

use Phalcon\Http\Response\Cookies     as Cookies,
    Phalcon\Crypt                     as Crypt,
    Phalcon\Mvc\Dispatcher\Exception  as DispatchException,
    Phalcon\Db\Adapter\Pdo\Mysql      as DbAdapter,
    Phalcon\Events\Manager            as EventsManager,
    Phalcon\Mvc\Dispatcher            as PhDispatcher,
    Phalcon\Session\Adapter\Stream    as SessionAdapter,
    Phalcon\Session\Manager           as SessionManager,
    Phalcon\Url                       as UrlResolver,
    Phalcon\DI\FactoryDefault,
    Phalcon\Mvc\Router,
    Phalcon\Dispatcher;

use PHPMailer\PHPMailer\PHPMailer;

/**
 * @param object $di Generates new Phalcon FactoryDefault
 * @param object $cf Generates new configuration object from json
**/

$di = new FactoryDefault();
$cf = (Object)json_decode(file_get_contents( __DIR__ . "/../config/config.json"));

/**
 * @param object $di['db'] Generates new Function that returns a PDO MSQL Connection
 * @param object $cf gets configs from json
**/

$di['db'] = function() use ($cf) {

    return new DbAdapter([
      "host"      => $cf->database->host,
      "username"  => $cf->database->username,
      "password"  => $cf->database->password,
      "dbname"    => $cf->database->dbname,
      "charset"   => $cf->database->charset
    ]);

};

/**
 * @param object $di['url'] Generates new Url resolver
**/

$di['url'] = function () {
    $url = new UrlResolver();
    $url->setBaseUri('/');

    return $url;
};

/**
 * @param object $di['session'] Creates new Session Adapter and initiates session
**/

$di['session'] = function (){
  $session = new SessionManager();
  $files = new SessionAdapter([
      'savePath' => sys_get_temp_dir(),
  ]);
  $session->setAdapter($files);
  $session->start();

  return $session;
};

/**
 * @param object $di['cookies'] Generates new site cookies and suses Crypt for encryption
 * @param object $di['crypt'] sets key for cookie
**/

$di['cookies'] = function() {
    $cookies = new Cookies();
    $cookies->useEncryption(true);
    return $cookies;
};

$di['crypt'] = function() use ($cf) {
    $crypt = new Crypt();
    $crypt->setKey( md5($cf->database->host) );
    return $crypt;
};

/**
 * @param object $di['configuration'] returns some values from config.json to be used in application
**/

$di['configuration'] = function () use ($cf) {
    return (object)[
      "database"      => $cf->database ,
      "mail"          => $cf->mail,
      "debug"         => $cf->server->debug,
    ];
};


/**
 * @param object $di['dispatcher'] handles 404 page
**/

$di['dispatcher'] = function () {
    $eventsManager = new EventsManager();
    $eventsManager->attach("dispatch:beforeException", function($event, $dispatcher, $exception) {

      if ($exception instanceof DispatchException) {
        $dispatcher->forward(array(
          'controller' => 'ErrorHandler',
          'action'     => 'notfound'
        ));
        return false;
      }

    });

    $dispatcher = new PhDispatcher();
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
};

/**
 * @param object $di['mail'] Configures PHPMailer ( loaded by Composer )  returning the package functions for use in application
**/

$di['mail'] = function () use ($cf) {
    $mail = new PHPMailer(true);

    $mail->isSMTP();
    $mail->isHTML(true);

    $mail->CharSet      = $cf->mail->charset;
    $mail->Host         = $cf->mail->host;
    $mail->SMTPAuth     = true;
    $mail->From         = $cf->mail->username;
    $mail->Username     = $cf->mail->username;
    $mail->Password     = $cf->mail->password;
    $mail->SMTPSecure   = $cf->mail->security;
    $mail->Port         = $cf->mail->port;

    return $mail;
};

/**
 * @param object $di['router'] Initializes Router and sets default namespace used
**/

$di['router'] = function () use ($di){
    $router = new Router();

    # check if login session is active
    $session = $di['session'];
    $active_session = $session->has("vhf_session");

    $router->setDefaultModule('Website');
    $router->setDefaultNamespace('Website\Controllers');
    $router->removeExtraSlashes(true);

    require( __DIR__ . "/routes.php" );

    return $router;
};

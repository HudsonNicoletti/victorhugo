<?php
/**
 * @author Hudson Nicoletti <hudsonnicoletti.com>
 * @copyright Copyright (c) 2021, Hudson Nicoletti
 * @version 3.0
**/

$application->registerModules([
  'Website' => [
    'className' => 'Website\Module',
    'path'      => __DIR__ . '/../apps/Website/Module.php'
  ]
]);

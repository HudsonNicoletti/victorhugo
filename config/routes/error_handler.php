<?php
/**
 * @author Hudson Nicoletti <hudsonnicoletti.com>
 * @copyright Copyright (c) 2021, Hudson Nicoletti
 * @version 3.0
**/

$router->add("/servererror", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'ErrorHandler',
  'action'     => 'servererror'
]);

$router->add("/notfound", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'ErrorHandler',
  'action'     => 'notfound'
]);

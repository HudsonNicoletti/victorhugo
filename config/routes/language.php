<?php
/**
 * @author Hudson Nicoletti <hudsonnicoletti.com>
 * @copyright Copyright (c) 2021, Hudson Nicoletti
 * @version 3.0
**/

$router->add("/english", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'Language',
  'action'     => 'change',
  'language'   => 'en'
]);

$router->add("/portuguese", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'Language',
  'action'     => 'change',
  'language'   => 'pt'
]);

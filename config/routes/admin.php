<?php
/**
 * @author Hudson Nicoletti <hudsonnicoletti.com>
 * @copyright Copyright (c) 2021, Hudson Nicoletti
 * @version 3.0
**/

$router->add("/admin", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => ($active_session) ? "index" : "login"
]);

$router->add("/admin/login", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => "auth"
])->via(["POST"]);

$router->add("/admin/logout", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => 'logout'
]);

$router->add("/admin/portfolio", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => "portfolio"
]);

$router->add("/admin/portfolio/new", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => "new"
]);

$router->add("/admin/portfolio/create", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => "create"
])->via(["POST"]);

$router->add("/admin/portfolio/edit/{slug:[a-zA-Z0-9\_\-]+}", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => 'edit'
]);

$router->add("/admin/portfolio/update", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => "update"
])->via(["POST"]);

$router->add("/admin/portfolio/remove/{slug:[a-zA-Z0-9\_\-]+}", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => 'remove'
]);

$router->add("/admin/portfolio/delete", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'admin',
  'action'     => "delete"
])->via(["POST"]);

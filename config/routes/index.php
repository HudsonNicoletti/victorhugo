<?php
/**
 * @author Hudson Nicoletti <hudsonnicoletti.com>
 * @copyright Copyright (c) 2021, Hudson Nicoletti
 * @version 3.0
**/
$router->add("/portfolio/{slug:[a-zA-Z0-9\_\-\.]+}", [
    "namespace"  => "Website\Controllers",
    "module"     => "Website",
    'controller' => 'index',
    'action'     => 'portfolioItem'
  ]);

  $router->add("/about", [
    "namespace"  => "Website\Controllers",
    "module"     => "Website",
    'controller' => 'index',
    'action'     => 'about'
  ]);

  $router->add("/contact", [
    "namespace"  => "Website\Controllers",
    "module"     => "Website",
    'controller' => 'index',
    'action'     => 'contact'
  ]);

  $router->add("/send", [
    "namespace"  => "Website\Controllers",
    "module"     => "Website",
    'controller' => 'index',
    'action'     => 'send'
  ]);
